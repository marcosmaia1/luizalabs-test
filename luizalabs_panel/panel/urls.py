"""luizalabs_panel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from . import views

app_name = 'panel'

urlpatterns = [
    path('dashboard', views.dashboard, name='dashboard'),
    path('', views.loginIndex, name='loginIndex'),
    path('login', views.login, name='login'),
    path('api/login', views.loginApi, name='loginApi'),
    
#     path('getemployee/<str:employee_id>/', views.getEmployee, name='getEmployee'),
    path('getemployeelist', views.getEmployeeList, name='getEmployeeList'),
    
    path('api/addedituser', views.addEditUserApi, name='addEditUserApi'),
    
    path('api/getemployee/<str:employee_id>/', views.getEmployeeApi, name='getEmployeeApi'),
    path('api/getemployeelist/', views.getEmployeeListApi, name='getEmployeeListApi'),
    path('api/addeditemployee/', views.addEditEmployeeApi, name='addEditEmployeeApi'),
    path('api/removeemployee/<str:employee_id>/', views.removeEmployeeApi, name='removeEmployeeApi'),
]
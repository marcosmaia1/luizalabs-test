from django.http import HttpResponse
import pymongo
import json
import logging
from django.shortcuts import render
from bson.objectid import ObjectId
import traceback
from datetime import datetime
import os
import binascii
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

connection = pymongo.MongoClient('127.0.0.1', 27017) # LOCAL / PROD

db = connection["PanelApp"]

User = db['User']
Employee = db['Employee']
Token = db['Token']

commonFields = ['created', 'updated']
userFields = ['name', 'email', 'username', 'password', 'enabled', 'user_type']+commonFields
employeeFields = ['name', 'email', 'departament']+commonFields
tokenFields = ['user_id', 'user_type']+commonFields

#user_type
user_admin_type = 0
user_manager_type = 1

#VerifyRequest
def verifyRequest(required):
    def decorator(func):
        def wrapper(*args, **kwargs):
            request = args[0]
            logging.info("Request: "+str(request))
            if 'photo' in request.FILES or request.method == 'DELETE':
                dictionary = request.GET.dict()
            elif request.method == 'POST' or request.method == 'PUT':
                dictionary = request.POST.dict()
                try:
                    postDict = json.loads(request.body)
                    dictionary.update(postDict)
                except:
                    pass
                
                try:
                    getDict = request.GET.dict()
                    dictionary.update(getDict)
                except:
                    pass
                    
            elif request.method == 'GET' or request.method == 'DELETE':
                dictionary = request.GET.dict()
                
            args += (dictionary,)

            for i in required:
                if i == 'token':
                    if 'HTTP_AUTHORIZATION' not in request.META:
                        if 'token' not in dictionary:
                            return makeResponse(False, data={}, error="Parameter not found: token")
                        else:
                            token = dictionary['token']
                    else:
                        token = request.META['HTTP_AUTHORIZATION']
                        dictionary['token'] = token

                    
                    tokenDB = Token.find_one({'_id' : token})
                    if tokenDB == None:
                        return makeResponse(False, data={}, error="Token not found!")
                    else:
                        requested_by = User.find_one({"$and" : [{'_id' : ObjectId(tokenDB['user_id'])}, {'enabled' : True}]})
                        if requested_by == None:
                            return makeResponse(False, data={}, error="Admin not found.")
                        args += (requested_by,)
                elif i not in dictionary:
                    printDict(dictionary.copy())
                    return makeResponse(False, data={}, error ="Parameter not found: " + i)
                
            printDict(dictionary.copy())
            return func(*args, **kwargs)
        return wrapper
    return decorator

def printDict(dictionary):
#Censura informações sensíveis do dictionary para registro do log
    try:
        blackList = ['password']
        for field in dictionary:
            if field in blackList:
                dictionary[field] = u'*********'
                 
        logging.info("dictionary: "+str(dictionary))
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(dictionary))

def makeResponse(ok, **kwargs):
    response = {}
    response['ok'] = ok
    for i in kwargs:
        response[i] = kwargs[i]
    
    if 'data' not in response:
        response['data'] = {}    
    if 'error' not in response:
        response['error'] = ''
    if 'error_list' not in response:
        response['error_list'] = []
    return json.dumps(response)

def JSONResponse(func):
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        responseDict = json.loads(response)
        if 'status' in responseDict:
            return HttpResponse(response, content_type = "application/json;charset=utf-8", status = int(responseDict['status']))
        else:
            return HttpResponse(response, content_type = "application/json;charset=utf-8")
    return wrapper

def CreateToken(user_id, user_type):
    token = {
        '_id' : (binascii.b2a_hex(os.urandom(20))).decode('ascii'),
        'user_id' : str(user_id),    
        'user_type' : user_type,
        'created' : datetime.now(),
    }
    Token.save(token)
    return token['_id']

def ReturnUserToken(token):
    try:
        tokenDB = Token.find_one({'_id' : token})
        if tokenDB == None:
            return redirect('panel:loginIndex')
        
        user = User.find_one({"$and" : [{'_id' : ObjectId(tokenDB['user_id'])}, {'enabled' : True}]})
        if user == None:
            return redirect('panel:loginIndex')
           
        user.update({
            'created' : str(user['created']),
            'updated' : str(user['updated']),
            '_id' : str(user['_id']),
            'user_id' : str(user['_id']),
            'token' : token
        })
        user.pop('password', None)
        return user
    except Exception as e:
        logging.error(e)
        return redirect('panel:loginIndex')
    
def loginIndex(request):
    if User.find({}).count() == 0:
        user = {
            'name': 'Luke Skywalker',
            'username': 'luke',
            'password': '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4',
            'email': 'luke@gmail.com',
            'enabled': True,
            'user_type': 0,
            'created': datetime.now(),
            'updated': datetime.now()
        }
        User.save(user)
        request.session['session_error'] = 'Default user created. Please use username:luke/pass:1234'
        
        
    if 'session_error' in request.session and request.session['session_error']:
        context = {
            'login_data' : {
                'ok' : False,
                'error' : 'Session expired.',
                'error_list' : [request.session['session_error']]
            }
        }
        return render(request, 'panel/login.html', context)
    else:
        request.session['session_error'] = False
        return render(request, 'panel/login.html')

def dashboard(request):
    if 'token' not in request.session:
        request.session['session_error'] = True
        return redirect('panel:loginIndex')
    user = ReturnUserToken(request.session['token'])
    context = {
        'user_data': user,
    }
    return render(request, 'panel/index.html', context)

@verifyRequest(['username', 'password'])
def login(request, dictionary):
    try:
        login = LoginPrivate(dictionary)
        if login['ok']:
            request.session['token'] = login['data']['token']
            return redirect('panel:dashboard')
        else:
            context = {
                'login_data': login
            }
            return render(request, 'panel/login.html', context)        
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(dictionary))
        return makeResponse(False, data={}, error='Something strange happened.')


@csrf_exempt 
@JSONResponse
@verifyRequest(['username', 'password'])
def loginApi(request, dictionary):
    try:
        login = LoginPrivate(dictionary)
        if login['ok']:
            return makeResponse(True, data=login['data'])
        else:
            return makeResponse(False, data=login['data'], error=login['error'], error_list=login['error_list'])
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(dictionary))
        return makeResponse(False, data={}, error='Something strange happened.')

def LoginPrivate(dictionary):
    try:
        errors = []
        if dictionary['username'] in ['', None]:
            errors.append('Username is empty.')
        if dictionary['password'] in ['', None]:
            errors.append('Password is empty.')
        
        if errors != []:
            return {'ok' : False, 'data' : {}, 'error': '', 'error_list': errors}
        
        user = User.find_one({ '$and':[
                {'username' : {"$regex" : dictionary['username'].replace(' ', ''), "$options" : "-i"}},
                {'password' : dictionary['password']},
                {'enabled' : True}
            ]
        })
        
        if user == None:
            return {'ok' : False, 'data' : {}, 'error': 'Invalid username or password.', 'error_list':[]}
        else:
            if 'token' not in dictionary:
                token = CreateToken(str(user['_id']), user['user_type'])
            else:
                token = dictionary
                
            user.update({
                'created' : str(user['created']),
                'updated' : str(user['updated']),
                '_id' : str(user['_id']),
                'user_id' : str(user['_id']),
                'token' : str(token)
            })
            user.pop('password', None)
            return {'ok' : True, 'data' : user, 'error': '', 'error_list':[]}
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(dictionary))
        return {'ok' : False, 'data' : {}, 'error': 'Something strange happened.', 'error_list':[]}
    
    
@csrf_exempt
@JSONResponse
@verifyRequest(['token', 'name', 'email', 'username', 'password', 'user_type'])
def addEditUserApi(request, dictionary, user):
    try:
        errors = []
        if dictionary['name'] in ['', None]:
            errors.append('Name is empty.')
        if dictionary['username'] in ['', None]:
            errors.append('Username is empty.')
        if dictionary['email'] in ['', None]:
            errors.append('Email is empty.')
        if dictionary['password'] in ['', None]:
            errors.append('Password is empty.')
        if 'user_id' in dictionary:
            if dictionary['enabled'] not in [True, False]:
                errors.append('Enabled is empty.')
        else:    
            dictionary['enabled'] = True
            
        user = {
            'name': dictionary['name'],
            'username': dictionary['username'],
            'password': dictionary['password'],
            'email': dictionary['email'],
            'enabled': dictionary['enabled'],
            'user_type': dictionary['user_type'],
            'created': datetime.now(),
            'updated': datetime.now()
        }
        
        User.save(user)
        user.update({
            'created': str(user['created']),
            'updated': str(user['updated']),
            '_id': str(user['_id']),
            'user_id': str(user['_id']),
        })
        
        return makeResponse(True, data=user)
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(dictionary))
        return makeResponse(False, error='Something strange happened.')
    
@JSONResponse
@verifyRequest(['token', 'name', 'email', 'departament'])
def addEditEmployeeApi(request, dictionary, user):
    try:
        errors = []
        if dictionary['name'] in ['', None]:
            errors.append('Name is empty.')
        if dictionary['email'] in ['', None]:
            errors.append('Email is empty.')
        if dictionary['departament'] in ['', None]:
            errors.append('Departament is empty.')
            
            
        if 'employee_id' in dictionary and dictionary['employee_id'] not in ['', None]:
            employee = Employee.find_one({'_id': ObjectId(dictionary['employee_id'])})
            if employee == None:
                return makeResponse(False, error='Employee not found. Please reload the page and try again.')
        else:    
            employee = {
                'created': datetime.now(),
            }
            
        employee.update({
            'name': dictionary['name'],
            'departament': dictionary['departament'],
            'email': dictionary['email'],
            'updated': datetime.now()
        })
        
        Employee.save(employee)
        employee.update({
            'created': str(employee['created']),
            'updated': str(employee['updated']),
            '_id': str(employee['_id']),
            'employee_id': str(employee['_id']),
        })
        
        return makeResponse(True, data=employee)
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(dictionary))
        return makeResponse(False, error='Something strange happened.')
    
@JSONResponse
def removeEmployeeApi(request, employee_id):
    try:
        Employee.remove({'_id': ObjectId(employee_id)})
        return makeResponse(True, data='Employee removed')
    except Exception as e:
        logging.error(e)
        logging.info("dictionary: "+str(id))
        return makeResponse(False, error='Something strange happened.')

# def addEditEmployeeForm(request, employee_id=None):
#     if employee_id in ['', None]:
#         context = {
#             'employee_data': {
#                 'employee_id' : '',
#                 'name' : '',
#                 'email' : '',
#                 'departament' : ''
#             },
#         }
#         return render(request, 'panel/employee/detail.html', context)
#     else:
#         employeeList = getEmployeePrivate(employee_id=employee_id)
#         if employeeList['ok']:
#             context = {
#                 'employee_data': employeeList,
#             }
#             return render(request, 'panel/employee/detail.html', context)
#         else:
#             employeeListNew = getEmployeePrivate()
#             context = {
#                 'employee_list': employeeListNew,
#                 'error_msg' : employeeList['error']
#             }
#             return render(request, 'panel/employee/list.html', context)

def getEmployeeList(request):
    employeeList = getEmployeePrivate()
    if 'token' not in request.session:
        request.session['session_error'] = True
        return redirect('panel:loginIndex')
    
    user = ReturnUserToken(request.session['token'])
    context = {
        'employee_list': employeeList,
        'user_data': user
    }
    return render(request, 'panel/employee/list.html', context)

@JSONResponse
def getEmployeeApi(request, employee_id):
    employeeList = getEmployeePrivate(employee_id=employee_id)
    if employeeList['ok']:
        return makeResponse(True, data=employeeList['data'])
    else:
        return makeResponse(False, data=employeeList['data'], error=employeeList['error'])

@JSONResponse
@verifyRequest(['token'])
def getEmployeeListApi(request, dictionary, user):    
    employeeList = getEmployeePrivate()
    if employeeList['ok']:
        return makeResponse(True, data=employeeList['data'])
    else:
        return makeResponse(False, data=employeeList['data'], error=employeeList['error'])

def getEmployeePrivate(employee_id = None):
    try:
        if employee_id in ['', None]:
            employeeDB = Employee.find()
            employeeList = []
            for employee in employeeDB:
                employee['_id'] = str(employee['_id'])
                employee['employee_id'] = str(employee['_id'])
                employeeList.append(employee)
            return {'ok' : True, 'data' : employeeList, 'error': ''}
        else:
            employee = Employee.find_one({'_id' : ObjectId(employee_id)})
            if employee == None:
                return {'ok' : False, 'data' : None, 'error': 'Employee not found.'}
            
            employee.update({
                'created': str(employee['created']),
                'updated': str(employee['updated']),
                '_id': str(employee['_id']),
                'employee_id': str(employee['_id']),
            })
            return {'ok' : True, 'data' : employee, 'error': ''}
    except:
        logging.exception(traceback.format_exc())
        return {'ok' : False, 'data' : [], 'error': 'Employee not found. Invalid id'}
    
    